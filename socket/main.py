import os
import asyncio
import tornado.web
from threading import Thread
import socketio
import pika
import time
import json
from messanger.receiver import Receiver
from messanger.sender import Sender

mgr = socketio.AsyncAioPikaManager('amqp://rabbit')
sio = socketio.AsyncServer(async_mode='tornado', cors_allowed_origins='*', client_manager=mgr)


class WebServer(tornado.web.Application):
    def __init__(self):
        self.port = os.environ['PORT']
        self.tower_name = os.environ['TOWER_NAME']
        handlers = [(r"/socket.io/", socketio.get_tornado_handler(sio)), ]
        settings = {'debug': True}
        super().__init__(handlers, **settings)

    def run(self):
        self.listen(self.port)
        tornado.ioloop.IOLoop.instance().start()


class Tower:
    def __init__(self, health, opponent_health, shield):
        self.tower_name = os.environ['TOWER_NAME']
        self.opponent_name = 'Pocus' if (os.environ['TOWER_NAME'] == 'Hocus') else 'Hocus'
        self.defense = 0 if (os.environ['TOWER_NAME'] == 'Hocus') else 1
        self.tower_shield = shield


@sio.event
async def attack(sid, msg):
    msg["action"] = "attack"
    sender.publish(tower.opponent_name + '_tower', 'direct', 'to_attack', '')
    sender.publish('restlin', 'direct', 'defender_attacks', json.dumps(msg))


@sio.on("connect")
async def connect(sid, environ):
    sio.enter_room(sid, tower.tower_name)
    sender.publish('restlin', 'direct', 'connect', "")


@sio.event
async def disconnect(sid):
        sender.publish('restlin', 'direct', 'disconnect', json.dumps({"tower_name": tower.tower_name}))



@sio.event
async def defend(sid, msg):
    tower.tower_shield += 150
    msg["action"] = "defend"
    sender.publish('restlin', 'direct', 'defender_defend', json.dumps(msg))
    await sio.emit("shield", json.dumps({"shield": tower.tower_shield}), to=tower.tower_name)


def start_server():
    asyncio.set_event_loop(asyncio.new_event_loop())
    ws.run()


def func1():
    while True:
        print('Working 1')
        time.sleep(8)


if __name__ == "__main__":
    sender = Sender()
    ws = WebServer()
    tower = Tower(5000, 5000, 0)
    receiver = Receiver(tower.tower_name + '_tower', 'direct', ['update', 'to_attack'], tower, sender)
    targets = [start_server, func1, receiver.setup]

    for target in targets:
        t = Thread(target=target, args=())
        t.setDaemon(False)
        t.start()
