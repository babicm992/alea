import pika
import sys
import os


class Sender():
    def __init__(self):
        self.host = 'rabbit'
        self.port = 5672
        self.exchange = ''

    def create_connection(self):
        param = pika.ConnectionParameters(host=self.host, port=self.port)
        return pika.BlockingConnection(param)

    def publish(self, exchange, exchange_type, routing_key, message):
        connection = self.create_connection()
        channel = connection.channel()
        channel.exchange_declare(exchange=exchange, exchange_type=exchange_type)

        channel.basic_publish(exchange=exchange, routing_key=routing_key, body=message)
        print(' [x] Sent message %r for %r' % (message, routing_key))
