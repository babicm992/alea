# coding: utf-8
from sqlalchemy import BigInteger, Boolean, CHAR, Column, DateTime, Date, ForeignKey, ForeignKeyConstraint, \
    Integer, String, Table, Text, UniqueConstraint, Numeric, text, Enum
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from sqlalchemy import extract
import datetime
from datetime import datetime as dt

import enum

Base = declarative_base()
metadata = Base.metadata


class TowerEnum(enum.Enum):
    Hocus = 0
    Pocus = 1


class TowerTable(Base):
    __tablename__ = 'tower'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    health = Column(Integer)
    defense = Column(Integer)
    session = Column(Integer, ForeignKey('session.id', ondelete='CASCADE'))
    defender_count = Column(Integer)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class SessionTable(Base):
    __tablename__ = 'session'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    time_created = Column(DateTime(timezone=True), default=func.now())
    global_defender_count = Column(Integer)

    hocus_tower_id = Column(Integer, ForeignKey('tower.id', ondelete='CASCADE'))
    pocus_tower_id = Column(Integer, ForeignKey('tower.id', ondelete='CASCADE'))
    hocus_tower = relationship('TowerTable', foreign_keys=[hocus_tower_id])
    pocus_tower = relationship('TowerTable', foreign_keys=[pocus_tower_id])


class DefenderTable(Base):
    __tablename__ = 'defender'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    nickname = Column(Text)
    attack_points_generated = Column(Integer)
    defense_points_generated = Column(Integer)
    tower_id = Column(Integer, ForeignKey('tower.id', ondelete='CASCADE'))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
